package com.vbenders.jee7.jaxrs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;

/**
 * Created by volker on 30.03.14.
 */
@Path(FileUploadController.PATH)
public class FileUploadController {
    public static final String PATH = "/file";
    public static final String UPLOAD_PATH = "/upload";
    public static final String UPLOADED_FILE_PARAMETER_NAME = "file";
    public static final String UPLOAD_DIR = "/tmp";
    private String data;
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadController.class);
    private MetricRegistry registry;
    private Timer uploadDuration;
    private Long uploadCounter;

    @PostConstruct
    public void initMetrics() {
        uploadCounter = 0L;
        registry = new MetricRegistry();
        startReport();
        uploadDuration = registry.timer(MetricRegistry.name(FileUploadController.class, "upload-duration"));
    }

    void startReport() {
        ConsoleReporter reporter = ConsoleReporter.forRegistry(registry).convertRatesTo(TimeUnit.SECONDS).convertDurationsTo(TimeUnit.MILLISECONDS).build();
        reporter.start(20, TimeUnit.SECONDS);
    }

    @Path(UPLOAD_PATH)
    @POST
    @Consumes("multipart/form-data")
    public Response uploadFile(MultipartFormDataInput input) {
        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
        List<InputPart> inputParts = uploadForm.get(UPLOADED_FILE_PARAMETER_NAME);

        for (InputPart inputPart : inputParts) {
            MultivaluedMap<String, String> headers = inputPart.getHeaders();
            String filename = getFileName(headers);

            Timer.Context context = uploadDuration.time();
            try {
                InputStream inputStream = inputPart.getBody(InputStream.class, null);

                final byte[] bytes = IOUtils.toByteArray(inputStream);

                LOGGER.info(">>> File '{}' has been read, size: #{} bytes", filename, bytes.length);
                writeFile(bytes, getServerFilename(filename));
                
                registry.register(MetricRegistry.name(FileUploadController.class, "file-upload-controller", "uploadFileSize"), new Gauge<Integer>() {
                    @Override
                    public Integer getValue() {
                        return bytes.length;
                    }

                });
            } catch (IOException e) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
            } finally {
                context.stop();
            }
        }
        return Response.status(Response.Status.OK).build();
    }

    /**
     * Build filename local to the server.
     * 
     * @param filename
     * @return
     */
    private String getServerFilename(String filename) {
        return UPLOAD_DIR + "/" + filename;
    }

    private void writeFile(byte[] content, String filename) throws IOException {
        LOGGER.info(">>> writing #{} bytes to: {}", content.length, filename);
        File file = new File(filename);

        if (!file.exists()) {
            file.createNewFile();
        }

        FileOutputStream fop = new FileOutputStream(file);

        fop.write(content);
        fop.flush();
        fop.close();
        LOGGER.info(">>> writing complete: {}", filename);
    }

    /**
     * Extract filename from HTTP heaeders.
     * 
     * @param headers
     * @return
     */
    private String getFileName(MultivaluedMap<String, String> headers) {
        String[] contentDisposition = headers.getFirst("Content-Disposition").split(";");

        for (String filename : contentDisposition) {
            LOGGER.info("ContentDisposition: {}", filename);
            if ((filename.trim().startsWith("filename"))) {

                String[] name = filename.split("=");

                String finalFileName = sanitizeFilename(name[1]);
                return finalFileName;
            }
        }
        return "unknown";
    }

    private String sanitizeFilename(String s) {
        return s.trim().replaceAll("\"", "");
    }

}
