README
======

JAX-RS file upload example. Developed for Wildfly 8 and Reasteasy.

Install
=======

There're two opton to install this artifact:

* build it and deploy it manually
```bash
[mupfel@volkers-imac]$~/Dev/workspace_kepler_01/FileUpload > mvn clean package
[mupfel@volkers-imac]$~/Dev/workspace_kepler_01/FileUpload > cp target/FileUpload.war $JBOSS_HOME/standalone/deployments
```

* build and deploy it maven style

The pom.xml contains the 'wildfly-maven-plugin'. The current setup assumes an environment variable points to your
local wildfly installation.
Maven supports global settings for all maven based projects. Those settings should be configured in 
~/.m2/settings.xml

```xml
	<profiles>
                <profile>
                        <id>wildfly-local</id>
                        <properties>
                                <wildfly.home>/usr/local/Cellar/wildfly-as/8.1.0.Final/libexec</wildfly.home>
                        </properties>
                </profile>
        </profiles>
```

By means of the plugin a simple call
```bash
[mupfel@volkers-imac]$~/Dev/workspace_kepler_01/FileUpload > mvn clean install
```
builds and deploys the module

Which one you choose is up to you - both get the job done.

Usage
===== 

```bash
curl -F file=@<FILE_NAME> http://localhost:8080/FileUpload/file/upload
```
Caveat: Mind the @-sign with the 'file' parameter - it tells curl to transfer the contents of the denoted file
and not the file name.